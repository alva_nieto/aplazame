#!/bin/bash

python setup.py build bdist_wheel
7z a aplazame.zip build_image.sh bootstrap.sh wait-for-it.sh Dockerfile docker-compose.yml dist/*.whl
scp -i aplazame.jobs aplazame.zip ubuntu@18.195.218.127:/home/ubuntu
