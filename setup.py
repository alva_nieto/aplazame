from setuptools import setup, find_packages


def requirements_file(filename='requirements.txt'):
    '''read a requirements file and create a list that can be used in setup.

    '''
    with open(filename, 'r') as f:
        return [x.rstrip() for x in list(f) if x and not x.startswith('#')]


setup(
    name='aplazame',
    version='1.0.0',
    author='Álvaro Nieto',
    author_email='alvaro.nieto@gmail.com',
    description='Aplazame herausforderung',
    packages=find_packages(),
    py_modules=['manage'],
    install_requires=requirements_file()
)
