import pytest

from rest_framework import serializers

from monedero.api.serializers.validator import belong_user_to_company, belong_user_to_customer


class TestCompany:

    def test_belong(self):
        assert belong_user_to_company(1, 1) is None

    def test_not_belong(self):
        with pytest.raises(serializers.ValidationError) as error:
            belong_user_to_company(1, 2)
        assert error.value.args[0] == 'User 1 can not access to company related to user 2'


class TestCustomer:

    def test_belong(self):
        assert belong_user_to_customer(1, 1) is None

    def test_not_belong(self):
        with pytest.raises(serializers.ValidationError) as error:
            belong_user_to_customer(1, 2)
        assert error.value.args[0] == 'User 1 can not access to customer related to user 2'
