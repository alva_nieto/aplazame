import os

import pytest

import django
from django.core import management


@pytest.fixture(autouse=True, scope='module')
def make_migrations():
    os.environ['DJANGO_SETTINGS_MODULE'] = 'aplazame.settings_test'
    django.setup()

    management.call_command('migrate')
