import pytest


from pytest_bdd import given, parsers
from django.test import Client


client = Client()
url_base = 'http://localhost:8000'


@pytest.fixture(scope='function')
def context():
    return {}


@given(parsers.parse('I am customer {name}'))
def customer(name, context):
    """I am customer name."""
    password = 'secreto1'
    response = create_customer(name, password)
    login(name, password)
    context['username'] = name
    return response


def create_customer(name, password):
    return _create_entity(name, password, 'customers')


def create_customer_account(customer_id):
    return _create_entity_account(customer_id, 'customers')


@given(parsers.parse('I am company {name}'))
def i_am_company(name, context):
    """I am company name."""
    password = 'secreto1'
    response = create_company(name, password)
    login(name, password)
    context['username'] = name
    return response


def create_company(name, password):
    return _create_entity(name, password, 'companies')


def create_company_account(company_id):
    return _create_entity_account(company_id, 'companies')


def login(username, password):
    client.post('/admin/login/', {'username': username, 'password': password})


def update_balance_customer(balance, customer_id, token):
    return _update_balance(balance, customer_id, token, 'customers')


def update_balance_company(balance, company_id, token):
    return _update_balance(balance, company_id, token, 'companies')


def get_company_account(company_id, token):
    return _get_account(company_id, token, 'companies')


def get_customer_account(customer_id, token):
    return _get_account(customer_id, token, 'customers')


def _get_account(id_, token, entity_name):
    return client.get(f'{url_base}/{entity_name}/{id_}/accounts/{token}/',
                      content_type='application/json').json()


def _update_balance(balance, id_, token, entity_name):
    payload = {
        'balance': balance
    }
    return client.put(f'{url_base}/{entity_name}/{id_}/accounts/{token}/', payload,
                      content_type='application/json')


def _create_entity(name, password, entity_name):
    payload = {
        'username': name,
        'password': password
    }
    response = client.post(f'{url_base}/{entity_name}/', payload, content_type='application/json')
    assert response.status_code == 201
    return response.json()


def _create_entity_account(id_, entity_name):
    return client.post(f'{url_base}/{entity_name}/{id_}/accounts/', content_type='application/json')
