from pytest_bdd import then, parsers
from django.contrib.auth import get_user_model
from django.db.models import Q


@then(parsers.parse('I get a {status_code:d}'))
def i_get_a_status_code(status_code, context):
    """I get a status_code."""
    assert context['status_code'] == int(status_code)
    get_user_model().objects.filter(~Q(username='ang')).delete()
