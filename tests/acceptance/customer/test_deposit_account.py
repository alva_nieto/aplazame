"""Deposit some money in a customer account feature tests."""

from decimal import Decimal

import pytest

from pytest_bdd import given, scenario, then, when, parsers

from ..steps.given import *
from ..steps.then import *


@scenario('deposit_account.feature', 'Deposit money')
def test_deposit_money():
    """Deposit money."""


@given(parsers.parse('I have an account with balance {balance:d}'))
def i_have_an_account_with_balance(balance, customer, context):
    """I have an account with balance."""
    response = create_customer_account(customer['id'])
    context['token'] = response.json()['token']
    update_balance_customer(balance, customer['id'], context['token'])


@when(parsers.parse('I make a deposit {amount:f} euros in the account through the application'))
def i_deposit_1002_euros_in_the_account_through_the_application(amount, customer, context):
    """I deposit 10.02 euros in the account throught the application."""
    response = update_balance_customer(amount, customer['id'], context['token'])
    context['status_code'] = response.status_code
    context['json'] = response.json()


@then(parsers.parse('My balance is {balance:f}'))
def my_balance_is(balance, customer, context):
    """my balance is"""
    response = client.get(f'{url_base}/customers/{customer["id"]}/accounts/{context["token"]}/',
                          content_type='application/json')
    context['json']['transactions'] = response.json()['transactions']
    assert Decimal(response.json()['balance']) == pytest.approx(Decimal(balance))


@then(parsers.parse('I have a transaction with amount {amount:f}'))
def i_have_a_transaction_with_amount(amount, context):
    """I have a transaction with amount."""
    assert Decimal(context['json']['transactions'][-1]['amount']) == pytest.approx(Decimal(amount))
