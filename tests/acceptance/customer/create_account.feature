Feature: Create an account linked to a customer
    As a registered Customer
    I want to create an account in the app


Scenario: Create an account
    Given I am customer federico
    When I create an account in the application
    Then I get a 201
    And I get a valid token
    Then I get a balance 0
