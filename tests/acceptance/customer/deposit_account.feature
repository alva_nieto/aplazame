Feature: Deposit some money in a customer account
    As a registered Customer
    I want to make a deposit in an account in the app


Scenario: Deposit money
    Given I am customer federico
    And I have an account with balance 100
    When I make a deposit 10.02 euros in the account through the application
    Then I have a transaction with amount 10.02
    And My balance is 110.02
    And I get a 200
