"""Register a customer feature tests."""

from django.contrib.auth import get_user_model

from pytest_bdd import given, scenario, when, parsers
from ..steps.given import *
from ..steps.then import *


@scenario('create_customer.feature', 'Create a new customer')
def test_create_a_new_customer():
    """Create a new customer."""


@scenario('create_customer.feature', 'Try to create an existing customer')
def test_try_to_create_an_existing_customer():
    """Try to create an existing customer."""


@given(parsers.parse('an existing customer {name}'))
def an_existing_customer_name(name):
    """an existing customer name."""
    get_user_model().objects.create(username=name, password='foo1234455')


@given(parsers.parse('I am customer {name}'))
def i_am_customer_name(name, context):
    """I am customer name"""
    context['username'] = name


@when('I register in the application')
def create_customer(name, context):
    """I create the customer."""
    payload = {
        'username': name,
        'password': 'foo1234455'
    }
    result = client.post(f'{url_base}/customers/', data=payload)
    context['status_code'] = result.status_code
