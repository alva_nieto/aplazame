Feature: Register a customer
    As a Person
    I want to create a customer in the app


Scenario: Create a new customer
    Given I am customer federico
    When I register in the application
    Then I get a 201


Scenario: Try to create an existing customer
    Given an existing customer federico
    Given I am customer federico
    When I register in the application
    Then I get a 400
