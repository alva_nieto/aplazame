"""Create an account linked to a customer feature tests."""

from decimal import Decimal

from pytest_bdd import scenario, then, when, parsers

from ..steps.given import *
from ..steps.then import *


@scenario('create_account.feature', 'Create an account')
def test_create_an_account():
    """Create an account."""


@when('I create an account in the application')
def i_create_an_account_in_the_application(customer, context):
    """I create an account in the application."""
    result = create_customer_account(customer['id'])
    context['status_code'] = result.status_code
    context['json'] = result.json()


@then('I get a valid token')
def i_get_a_valid_token(context):
    """I get a valid token."""
    assert context['json'].get('token')


@then(parsers.parse('I get a balance {balance:d}'))
def i_get_a_balance_0(balance, context):
    """I get a valid token."""
    assert Decimal(context['json'].get('balance')) == Decimal(balance)
