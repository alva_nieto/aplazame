Feature: Charge money
    As a registered Company
    I want to make a charge in a customer account
    And a deposit in my account


Scenario: Charge money
    Given I am customer federico
    And I have an account with balance 100
    And I am company allado
    And I have a company account with balance 200
    When I make a charge of -10.02 euros through the application
    Then I have a transaction in company account with amount 10.02
    And I have a transaction in customer account with amount -10.02
    And My balance in company account is 210.02
    And My balance in customer account is 89.98
    And I get a 200
