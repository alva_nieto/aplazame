Feature: Create an account linked to a company
    As a registered Company
    I want to create an account in the app


Scenario: Create an account
    Given I am company allado
    When I create an account in the application
    Then I get a 201
    And I get a valid token
    Then I get a balance 0
