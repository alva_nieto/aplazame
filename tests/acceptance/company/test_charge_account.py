"""Charge money feature tests."""

from decimal import Decimal

import pytest

from pytest_bdd import given, scenario, then, when, parsers

from ..steps.given import *
from ..steps.then import *


@scenario('charge_account.feature', 'Charge money')
def test_charge_money():
    """Charge money."""


@given(parsers.parse('I have a company account with balance {balance:d}'))
def i_have_a_company_account_with_balance(balance, i_am_company, context):
    """I have a company account with balance."""
    response = create_company_account(i_am_company['id'])
    context['company_token'] = response.json()['token']
    update_balance_company(balance, i_am_company['id'], context['company_token'])


@given(parsers.parse('I have an account with balance {balance:d}'))
def i_have_an_account_with_balance(balance, customer, context):
    """I have an account with balance."""
    response = create_customer_account(customer['id'])
    context['customer_token'] = response.json()['token']
    update_balance_customer(balance, customer['id'], context['customer_token'])


@when(parsers.parse('I make a charge of {balance:f} euros through the application'))
def i_make_a_charge_of_1002_euros_through_the_application(balance, i_am_company, context):
    """I make a charge of balance euros through the application."""
    payload = {
        'balance': balance,
        'customer_token': context['customer_token']
    }
    response = client.put(f'{url_base}/companies/{i_am_company["id"]}/'
                          f'accounts/{context["company_token"]}/charge/', payload,
                          content_type='application/json')
    context['status_code'] = response.status_code
    context['json'] = response.json()


@then(parsers.parse('I have a transaction in company account with amount {amount:f}'))
def i_have_a_transaction_in_company_account_with_amount(amount, i_am_company, context):
    """I have a transaction in company account with amount."""
    account = get_company_account(i_am_company['id'], context['company_token'])
    assert Decimal(account[0]['transactions'][-1]['amount']) == pytest.approx(Decimal(amount))


@then(parsers.parse('I have a transaction in customer account with amount {amount:f}'))
def i_have_a_transaction_in_customer_account_with_amount(amount, customer, context):
    """I have a transaction in customer account with maount."""
    account = get_customer_account(customer['id'], context['customer_token'])
    assert Decimal(account['transactions'][-1]['amount']) == pytest.approx(Decimal(amount))


@then(parsers.parse('My balance in company account is {balance:f}'))
def my_balance_in_company_account_is(balance, i_am_company, context):
    """My balance in company account is."""
    account = get_company_account(i_am_company['id'], context['company_token'])
    assert Decimal(account[0]['balance']) == pytest.approx(Decimal(balance))


@then(parsers.parse('My balance in customer account is {balance:f}'))
def my_balance_in_customer_account_is(balance, customer, context):
    """My balance in customer account is."""
    account = get_customer_account(customer['id'], context['customer_token'])
    assert Decimal(account['balance']) == pytest.approx(Decimal(balance))
