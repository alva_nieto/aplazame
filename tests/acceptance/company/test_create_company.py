"""Register a company feature tests."""

from django.contrib.auth import get_user_model

from pytest_bdd import given, scenario, when, parsers
from ..steps.given import *
from ..steps.then import *


@scenario('create_company.feature', 'Create a new company')
def test_create_a_new_company():
    """Create a new company."""


@scenario('create_company.feature', 'Try to create an existing company')
def test_try_to_create_an_existing_company():
    """Try to create an existing company."""


@given(parsers.parse('an existing company {name}'))
def an_existing_company_name(name):
    """an existing company name."""
    get_user_model().objects.create(username=name, password='foo1234455')


@given(parsers.parse('I am company {name}'))
def i_am_company_name(name, context):
    """I am company name"""
    context['username'] = name


@when('I register in the application')
def create_company(name, context):
    """I create the company."""
    payload = {
        'username': name,
        'password': 'foo1234455'
    }
    result = client.post(f'{url_base}/companies/', data=payload)
    context['status_code'] = result.status_code
