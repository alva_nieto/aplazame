from rest_framework import status
from rest_framework.response import Response
from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny, IsAuthenticatedOrReadOnly

from .serializers.customer import (CustomerViewSerializer, CustomerCreateSerializer,
                                   CustomerAccountSerializer)
from ..models import Customer, CustomerAccount


@api_view(['GET', 'POST'])
@permission_classes((AllowAny,))
def customer(request):
    if request.method == 'GET':
        queryset = Customer.objects.all()
        serializer = CustomerViewSerializer(queryset, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = CustomerCreateSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET'])
@permission_classes((AllowAny,))
def customer_details(request, pk):
    if request.method == 'GET':
        queryset = Customer.objects.get(pk=pk)
        serializer = CustomerViewSerializer(queryset)
        return Response(serializer.data)


@api_view(['GET', 'POST'])
@permission_classes((IsAuthenticatedOrReadOnly,))
def customer_accounts(request, pk):
    if request.method == 'GET':
        queryset = CustomerAccount.objects.filter(customer=Customer.objects.filter(id=pk).first())
        serializer = CustomerAccountSerializer(queryset, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = CustomerAccountSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(customer_id=pk, user_id=request.user.id)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['GET', 'PUT'])
@permission_classes((IsAuthenticatedOrReadOnly,))
def customer_accounts_details(request, customer_pk, account_pk):
    if request.method == 'GET':
        queryset = CustomerAccount.objects.get(pk=account_pk)
        serializer = CustomerAccountSerializer(queryset)
        return Response(serializer.data)

    elif request.method == 'PUT':
        row = CustomerAccount.objects.get(pk=account_pk)
        serializer = CustomerAccountSerializer(row, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
