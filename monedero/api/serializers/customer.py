from decimal import Decimal

from django.db import transaction
from django.contrib.auth import get_user_model

from rest_framework import serializers

from monedero.models import Customer, CustomerAccount, CustomerAccountTransaction
from monedero.api.serializers.validator import belong_user_to_customer


User = get_user_model()


class CustomerAccountTransactionSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = CustomerAccountTransaction


class CustomerAccountSerializer(serializers.Serializer):
    token = serializers.CharField(required=False)
    balance = serializers.DecimalField(max_digits=12, decimal_places=2, required=False)
    customer_id = serializers.IntegerField(required=False)
    transactions = CustomerAccountTransactionSerializer(required=False, many=True)

    def create(self, validated_data):
        customer = Customer.objects.get(pk=validated_data['customer_id'])
        belong_user_to_customer(validated_data['user_id'], customer.user_id)

        return CustomerAccount.objects.create(customer=customer, balance=0)

    def update(self, instance, validated_data):
        total_balance = Decimal(validated_data['balance']) + instance.balance
        if total_balance < Decimal('0.0'):
            with transaction.atomic():
                CustomerAccountTransaction.objects.create(account=instance,
                                                          amount=validated_data['balance'])
            raise serializers.ValidationError(f'Total balance {total_balance} must be greater or '
                                              'equals to 0')
        with transaction.atomic():
            instance.balance += validated_data['balance']
            instance.save()
            CustomerAccountTransaction.objects.create(account=instance,
                                                      amount=validated_data['balance'])
        return instance


class CustomerCreateSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=False)
    username = serializers.CharField(max_length=150)
    password = serializers.CharField(min_length=8, max_length=20)

    def create(self, validated_data):
        if User.objects.filter(username=validated_data['username']).first():
            raise serializers.ValidationError('Customer {} exists'.
                                              format(validated_data['username']))
        user = User.objects.create_user(is_staff=True, **validated_data)
        return {
            'id': Customer.objects.create(user=user).id,
            'username': user.username,
            'password': user.password
        }


class CustomerViewSerializer(serializers.ModelSerializer):
    accounts = CustomerAccountSerializer(required=False, many=True)

    class Meta:
        fields = '__all__'
        model = Customer
