from rest_framework import serializers


def belong_user_to_company(user_id, user_company_id):
    _belong_user_to(user_id, user_company_id, 'company')


def belong_user_to_customer(user_id, user_customer_id):
    _belong_user_to(user_id, user_customer_id, 'customer')


def _belong_user_to(user_id, user_entity_id, entity_name):
    if user_id != user_entity_id:
        raise serializers.ValidationError(f'User {user_id} can not access to {entity_name} '
                                          f'related to user {user_entity_id}')
