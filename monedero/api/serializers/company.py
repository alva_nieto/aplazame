from decimal import Decimal

from django.db import transaction
from django.contrib.auth import get_user_model

from rest_framework import serializers

from monedero.models import (Company, CompanyAccount, CompanyAccountTransaction, CustomerAccount,
                             CustomerAccountTransaction)
from monedero.api.serializers.validator import belong_user_to_company


User = get_user_model()


class CompanyAccountTransactionSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = CompanyAccountTransaction


class CompanyAccountSerializer(serializers.Serializer):
    token = serializers.CharField(required=False)
    balance = serializers.DecimalField(max_digits=12, decimal_places=2, required=False)
    company_id = serializers.IntegerField(required=False)
    transactions = CompanyAccountTransactionSerializer(required=False, many=True)

    def create(self, validated_data):
        company = Company.objects.get(pk=validated_data['company_id'])
        belong_user_to_company(validated_data['user_id'], company.user_id)

        return CompanyAccount.objects.create(company=company, balance=0)

    def update(self, instance, validated_data):
        company = Company.objects.get(pk=instance.company_id)
        belong_user_to_company(validated_data['user_id'], company.user_id)

        total_balance = Decimal(validated_data['balance']) + instance.balance
        if total_balance < Decimal('0.0'):
            with transaction.atomic():
                CompanyAccountTransaction.objects.create(account=instance,
                                                         amount=validated_data['balance'])
            raise serializers.ValidationError(f'Total balance {total_balance} must be greater or '
                                              'equals to 0')
        with transaction.atomic():
            instance.balance += validated_data['balance']
            instance.save()
            CompanyAccountTransaction.objects.create(account=instance,
                                                     amount=validated_data['balance'])
        return instance


class CompanyCreateSerializer(serializers.Serializer):
    id = serializers.IntegerField(required=False)
    username = serializers.CharField(max_length=150)
    password = serializers.CharField(min_length=8, max_length=20)

    def create(self, validated_data):
        if User.objects.filter(username=validated_data['username']).first():
            raise serializers.ValidationError('Company {} exists'.
                                              format(validated_data['username']))
        user = User.objects.create_user(is_staff=True, **validated_data)
        return {
            'id': Company.objects.create(user=user).id,
            'username': user.username,
            'password': user.password
        }


class CompanyViewSerializer(serializers.ModelSerializer):
    accounts = CompanyAccountSerializer(required=False, many=True)

    class Meta:
        fields = '__all__'
        model = Company


class CompanyAccountChargeSerializer(serializers.Serializer):
    customer_token = serializers.CharField()
    balance = serializers.DecimalField(max_digits=12, decimal_places=2)

    def validate_balance(self, value):
        if Decimal(value) > Decimal('0.0'):
            raise serializers.ValidationError('Amount should be always negative because we only '
                                              'process charges not chargebacks')
        return value

    def update(self, instance, validated_data):
        company = Company.objects.get(pk=instance.company_id)
        belong_user_to_company(validated_data['user_id'], company.user_id)

        customer_account = CustomerAccount.objects.get(pk=validated_data['customer_token'])
        total_balance = Decimal(validated_data['balance']) + customer_account.balance
        if total_balance < Decimal('0.0'):
            with transaction.atomic():
                CustomerAccountTransaction.objects.create(account=customer_account,
                                                          amount=validated_data['balance'])
            raise serializers.ValidationError(f'Total balance of the customer {total_balance} '
                                              'after the charge must be greater or equals to 0')
        with transaction.atomic():
            instance.balance += abs(validated_data['balance'])
            instance.save()
            CompanyAccountTransaction.objects.create(account=instance,
                                                     amount=abs(validated_data['balance']))
            CustomerAccountTransaction.objects.create(account=customer_account,
                                                      amount=validated_data['balance'])
            balance = customer_account.balance + Decimal(validated_data['balance'])
            try:
                (CustomerAccount.objects.filter(pk=validated_data['customer_token'],
                                                updated=customer_account.updated)
                 .update(balance=balance))
            except AttributeError:
                raise serializers.ValidationError('The customer account has been updated by another'
                                                  ' transaction')
        return {
            'balance': instance.balance,
            'customer_token': validated_data['customer_token']
        }
