from django.urls import path

from ..customer import customer, customer_details, customer_accounts, customer_accounts_details


urlpatterns = [
    path('customers/', customer),
    path('customers/<int:pk>/', customer_details),
    path('customers/<int:pk>/accounts/', customer_accounts),
    path('customers/<int:customer_pk>/accounts/<str:account_pk>/', customer_accounts_details),
]
