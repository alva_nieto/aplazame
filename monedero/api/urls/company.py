from django.urls import path
from rest_framework import routers

from ..company import CompanyViewSet, CompanyAccountViewSet, ChargeViewSet


router = routers.DefaultRouter()
router.register(r'companies', CompanyViewSet)

urlpatterns = router.urls + [
    path('companies/<int:company_pk>/accounts/', CompanyAccountViewSet.as_view({'get': 'list',
                                                                                'post': 'create'})),
    path('companies/<int:company_pk>/accounts/<str:account_pk>/',
         CompanyAccountViewSet.as_view({'get': 'list', 'put': 'update'})),
    path('companies/<int:company_pk>/accounts/<str:account_pk>/charge/',
         ChargeViewSet.as_view({'put': 'charge'})),
]
