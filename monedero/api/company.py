from django.shortcuts import get_object_or_404
from rest_framework import status, viewsets
from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework.permissions import AllowAny, IsAuthenticatedOrReadOnly

from .serializers.company import (CompanyViewSerializer, CompanyAccountSerializer,
                                  CompanyCreateSerializer, CompanyAccountChargeSerializer)
from ..models import Company, CompanyAccount


class CompanyViewSet(viewsets.ModelViewSet):
    permission_classes = (AllowAny,)
    queryset = Company.objects.all()
    serializer_class = CompanyViewSerializer

    def create(self, request):
        serializer = CompanyCreateSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def retrieve(self, request, pk=None):
        queryset = Company.objects.all()
        company = get_object_or_404(queryset, pk=pk)
        serializer = CompanyViewSerializer(company)
        return Response(serializer.data)


class CompanyAccountViewSet(viewsets.ModelViewSet):
    permission_classes = (IsAuthenticatedOrReadOnly,)
    serializer_class = CompanyAccountSerializer

    def get_queryset(self):
        return CompanyAccount.objects.filter(company_id=self.kwargs['company_pk'])

    def create(self, request, company_pk):
        serializer = CompanyAccountSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(company_id=company_pk, user_id=request.user.id)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, company_pk, account_pk):
        row = CompanyAccount.objects.get(pk=account_pk)
        serializer = CompanyAccountSerializer(row, data=request.data)
        if serializer.is_valid():
            serializer.save(user_id=request.user.id)
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class ChargeViewSet(viewsets.ViewSet):
    permission_classes = (IsAuthenticatedOrReadOnly,)
    serializer_class = CompanyAccountSerializer

    def get_queryset(self):
        return CompanyAccount.objects.filter(pk=self.kwargs['account_pk'])

    @action(detail=True, methods=['put'])
    def charge(self, request, company_pk, account_pk):
        company_account = CompanyAccount.objects.get(pk=account_pk)
        serializer = CompanyAccountChargeSerializer(company_account, data=request.data)
        if serializer.is_valid():
            serializer.save(user_id=request.user.id)
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
