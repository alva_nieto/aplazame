from django.apps import AppConfig


class MonederoConfig(AppConfig):
    name = 'monedero'
