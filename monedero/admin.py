from django.contrib import admin

from .models import (Customer, CustomerAccount, CustomerAccountTransaction, Company,
                     CompanyAccount, CompanyAccountTransaction)


@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = ('id', 'username')

    def username(self, obj):
        return f'{obj.user.username}'


@admin.register(CustomerAccount)
class CustomerAccountAdmin(admin.ModelAdmin):
    list_display = ('token', 'balance', 'customer', 'updated')


@admin.register(CustomerAccountTransaction)
class CustomerAccountTransactionAdmin(admin.ModelAdmin):
    list_display = ('id', 'amount', 'account', 'operation_date')


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    list_display = ('id', 'username')

    def username(self, obj):
        return f'{obj.user.username}'


@admin.register(CompanyAccount)
class CompanyAccountAdmin(admin.ModelAdmin):
    list_display = ('token', 'balance', 'company')


@admin.register(CompanyAccountTransaction)
class CompanyAccountTransactionAdmin(admin.ModelAdmin):
    list_display = ('id', 'amount', 'account', 'operation_date')
