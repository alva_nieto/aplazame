import uuid

from django.utils import timezone
from django.db import models
from django.contrib.auth import get_user_model


class Customer(models.Model):

    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(get_user_model(), on_delete=models.CASCADE)

    def __str__(self):
        return f'{self.id} - {self.user.username}'


class CustomerAccount(models.Model):

    token = models.UUIDField(primary_key=True, default=uuid.uuid4, editable=False)
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE, related_name='accounts')
    balance = models.DecimalField(max_digits=12, decimal_places=2)
    updated = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return f'{self.token} - {self.balance}'


class CustomerAccountTransaction(models.Model):

    id = models.AutoField(primary_key=True)
    account = models.ForeignKey(CustomerAccount, on_delete=models.CASCADE,
                                related_name='transactions')
    amount = models.DecimalField(max_digits=12, decimal_places=2)
    operation_date = models.DateTimeField(default=timezone.now)
