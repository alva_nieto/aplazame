#!/bin/sh

./wait-for-it.sh database:5432 -- python manage.py migrate --noinput
python manage.py shell -c "from django.contrib.auth.models import User; User.objects.create_superuser('ang', 'ang@aplazame.com', 'ang')"
python manage.py runserver 0.0.0.0:8000
