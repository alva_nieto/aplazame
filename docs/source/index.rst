.. aplazame documentation master file, created by
   sphinx-quickstart on Sat Apr  6 12:14:16 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to aplazame's documentation!
====================================

.. toctree::
   :maxdepth: 2

   Clientes <customers.rst>
   Compañías <companies.rst>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
