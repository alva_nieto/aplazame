Comandos
========

Crear Cliente
-------------

.. uml::

   autonumber
   hide footbox
   skinparam monochrome false
   skinparam handwritten false
   
   skinparam sequence {
   	ArrowColor Black
   	LifeLineBorderColor Black
   
   	ParticipantBorderColor DimGray
   	ParticipantBackgroundColor Ivory
   
   	ActorBorderColor DimGray
   	ActorBackgroundColor Ivory
   
   	DatabaseBorderColor DimGray
   	DatabaseBackgroundColor Ivory
   }
   
   title __Create Customer__
   
   Cliente -> App: create
   App -> App: exists
   App -> Cliente: created
