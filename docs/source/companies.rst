Comandos
========

Cargo en cuenta de cliente
--------------------------

.. uml::

   autonumber
   hide footbox
   skinparam monochrome false
   skinparam handwritten false
   
   skinparam sequence {
   	ArrowColor Black
   	LifeLineBorderColor Black
   
   	ParticipantBorderColor DimGray
   	ParticipantBackgroundColor Ivory
   
   	ActorBorderColor DimGray
   	ActorBackgroundColor Ivory
   
   	DatabaseBorderColor DimGray
   	DatabaseBackgroundColor Ivory
   }
   
   title __Charge Processing__
   
   
   actor CompanyUser
   participant CompanyAccount
   participant CustomerAccount
   participant CompanyAccountTransactions
   participant CustomerAccountTransactions
   
   CompanyUser -> CompanyAccount: charge(user, amount, customer_token)
   
   activate CompanyAccount
   CompanyAccount -> CompanyAccount: user_authenticated(user)
   CompanyAccount -> CompanyAccount: user_belongs_to_company(user)
   alt yes
   CompanyAccount -> CustomerAccount: exists(customer_token)
   CompanyAccount -> CustomerAccount: validate_balance(amount)
   
   alt balance >= 0
       CompanyAccount -> CustomerAccount: update_balance(amount)
       CompanyAccount -> CustomerAccountTransactions: add_transaction(amount)
       CompanyAccount -> CompanyAccount: update_balance(amount)
       CompanyAccount -> CompanyAccountTransactions: add_transaction(amount)
   else balance < 0
       CompanyAccount --> CompanyUser: Exception('Not enough balance')
   end
   else no
       CompanyAccount --> CompanyUser: Exception('Invalid user')
   end
   deactivate CompanyAccount
