FROM python:3

WORKDIR /usr/local/lib/python3.7/site-packages

COPY wait-for-it.sh .
COPY bootstrap.sh .
COPY dist/aplazame-1.0.0-py3-none-any.whl /tmp
RUN pip install --no-cache-dir /tmp/aplazame-1.0.0-py3-none-any.whl
