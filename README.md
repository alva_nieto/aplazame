Configurar el entorno
=====================

Prerequisitos
-------------

* Python 3.7
* Linux (puede funcionar en otras plataformas, pero no está testeado)

Entorno de desarrollo
---------------------

Se recomienda utilizar un entorno virtual (o virtualenvwrapper) para gestionar las dependencias.

```bash
python setup.py develop
```

o

```bash
pip install -r requirements-dev.txt
```

Ejecutar los tests
------------------

Los tests se ejecutan con pytest (configuración en setup.cfg).

```bash
pytest tests
```

Los tests utilizan la configuración definida en aplazame.settings_test, que apunta a una base de
datos en local, si se quieren ejecutar los tests contra una base de datos ubicada en otro servidor
habría que ajustar la configuración o hacer port forwarding.

Cobertura de código
-------------------

Los informes de cobertura se encuentran en htmlcov/index.html y se crean automáticamente al ejecutar
los tests (configuración especificada en setup.cfg).

Ejeuctar la aplicación en local
-------------------------------

Se ejecutará como cualquier aplicación django.

```bash
python manage.py runserver
```

Documentación
=============

Documentación técnica
---------------------

Se ha creado documentación básica basada en sphinx-doc.

Es requisito, para generar los diagramas de secuencia basados en Plantuml, disponer de java 8+.

La generación de la documentación se basa en los mecanismos estándar de sphinx-doc;

```bash
cd docs
make html
firefox build/html/index.html
```

Documentación de API
--------------------

Se han generado dos versiones de la documentación de los servicios rest;

* drf-yasg -> url /swagger
* django-rest-framework-redocs -> url /redoc

CUESTIONES
==========

1. Depliegue de la aplicación
-----------------------------

La aplicación se puede desplegar de dos maneras.

Una primera un poco más manual basada en un artefacto wheel que se genera con la instrucción;

```bash
python setup.py build bdist_wheel
```

luego se puede instalar la aplicación en la máquina de despliegue;

```bash
pip install --user aplazame.whl
```

Estos pasos se pueden automatizar con un playbook de ansible, por ejemplo.

Otra opción es dockerizar la aplicación, publicar la imagen en un repositorio de imágemes (nexus,
por ejemplo) y hacer el pull en el servidor de despliegue.

Para esta prueba se ha creado un fichero Dockerfile para crear la imagen de la aplicación y un
fichero docker-compose.yml para;

* levantar la base de datos
* aplicar las migraciones
* crear un super usuario (ang/ang)
* levantar el servidor

Se ha creado el script deploy.sh que se encarga de crear un zip con los ficheros necesarios y los
copia con scp al servidor. En resumen, los pasos a seguir son;

* ejecutar en local deploy.sh
* descomprimir en remoto el fichero aplazame.zip
* ejecutar en remoto docker-compose down
* ejecutar en remoto build_image.sh
* ejecutar en remoto docker-compose up

Para lanzar los tests de aceptación en local contra la base de datos recien desplegada se puede
hacer port forwarding al puerto 5432;

```bash
ssh -i aplazame.jobs -fN -L 5432:localhost:5432 ubuntu@18.195.218.127
```

Evidentemente, también se puede utilizar port forwarding para probar la aplicación remota como si
estuviera en local;

```bash
ssh -i aplazame.jobs -fN -L 8000:localhost:8000 ubuntu@18.195.218.127
```

Otra opción más realista hubiera sido incluir los tests en la imagen docker de la aplicación y
ejecutarlos desde ahí. Ver el apartado tests de aceptación de la sección cuestiones para más
alternativas.

El despligue en un entorno de producción no utilizaría el servidor de desarrollo de django sino uno
de los diponibles en python; gunicorn o uwsgi, junto con un nginx que actuaría como proxy inverso.

2. Optimizar operaciones de listado de operaciones
--------------------------------------------------

La primera optimización es hacer el menor numero de peticiones al backend, recuperando en una sola
llamada los datos solicitados, con la paginación correspondiente. A nivel de base de datos se
traduce en hacer una sola query con las joins correspondientes a la tablas de detalle, es decir,
forzar al ORM a que haga un eager vs un lazy loading.

Si aun así el listado de operaciones es lento, la segunda opción es hacer un análisis de las
consultas que se están haciendo realmente. Se puede hacer un Explain Plan a nivel de base de datos
para ver si hay que crear o forzar algún índice.

Algunas otras alternativas se solapan con las propuestas del punto 3.

3. Optimizar operaciones del repositorio
----------------------------------------

Si la base de datos se convierte en el cuello de botella para operaciones de lectura y/o escritura,
se pueden utilizar estrategias CQRS (Command Query Responsability Segregation) con la idea de
separar las operaciones que afectan al estado de las entidades de las operaciones de consulta.

Una vez separadas las operaciones se puede utilizar un backend para las operaciones Command y otro
para las operaciones Query.

Por ejemplo, si tenemos una arquitectura de base de datos master/slave, se pueden ejecutar las
operaciones Command contra master y las operaciones Query contra slave.

Otra alternativa para acelerar las operaciones de consuluta es utilizar vistas materializadas (ya
sean a nivel de base de datos o a nivel de cache en memoria). Las vistas materializadas se encargan
de consolidar los datos que pueden estar dispersos en varias tablas y que utilizan directamente los
interfaces (gráficos, rest o lo que sea).

Otra opción, que leí hace poco en las news de Python Weekly / PyCoder's Weekly, es utilizar sharding
en la base de datos. Para la consulta de operaciones de una cuenta, lo más habitual es buscar las
operaciones más recientes, utilizando sharding para particionar la tabla de operaciones por fecha,
dejanto las más antiguas en las particiones menos usadas, se consigue que las búsquedas sean sobre
un número sensiblemente menor de registros. 

4. Usar NoSQL
-------------

El teorema de CAP dice que un sistema distribuido sólo puede garantizar simultáneamente 2 de las
siguientes 3 propiedades;

* Consistency
* Availability
* Partition Tolerance

Las bases de datos relacionales prefieren la consistencia sobre la disponibilidad, mientras que
NoSQL prefiere disponibilidad sobre consistencia. Como uno de los requisitos de negocio es asegurar
la consistencia de las operaciones de cargo en cuenta del cliente y abono en la cuenta de la
empresa, entonces es muy recomendable usar una base de datos relacional, por lo menos para las
operaciones Command. Las operaciones Query se pueden materializar en una NoSQL.

5. Métricas y servicios de monitorización
-----------------------------------------

Para monitorizar el estado de los servicios y repositorios de datos para tener métricas sobre uso de
hardware (memoria, disco, red, cpu, etc) se pueden utilizar herramientas tipo Prometheus o Graphite.

Las métricas más interesantes para los servicios suelen ser;

* Número de peticiones por segundo en función del tipo y del número de usuarios concurrentes de la
  aplicación
* Tiempo mínimo, máximo y medio de cada petición por tipo de petición y también en función de la
  carga del sistema

Para las métricas de servicio se puede utilizar Locust, JMeter o herramientas similares.

Tests de aceptación
-------------------

La idea orignal era hacer tests de aceptación independientes de django y la aplicación, es decir, la
aplicación se levantaría en local o un docker.

Los tests no deberían saber nada de django y la base de datos y deberían ser capaces de probar de
forma totalmente externa la aplicación. Empecé con esta aproximación pero se complicaba bastante,
sobre todo por la autenticación del usuario.

Al final, estos tests se han basado en la clase Client de django.tests.

Teniendo todos los tests se da uno cuenta de que los steps se pueden refactorizar para favorecer la
reutilización. Otro punto de mejora en los tests es el uso de funciones y variables auxiliares.
Ahora están todas en el módulo given, deberían ser menos mágicas y estar en otro módulo más
específico.

Diseño de la aplicación
-----------------------

El diseño de la aplicación es muy simple, la lógica de negocio está intimamente ligada a las
herramientas (django rest framework y base de datos). Para un proyecto pequeñito como este es
razonable, pero si las funcionalidades empezaran a aumentar sería muy recomendable empezar a usar
conceptos de diseño para aplicación más grandes.

En este ejemplo se empiezan a detectar los primeros modelos del dominio; Clientes y Empresas, así
como Cuentas y Transacciones. Además los Clientes y Empresas actuan como aggregates (siguiendo la
nomenclatura DDD - Domain Driven Design) ya que son el punto de entrada para las cuentas y las
transacciones.

Siguiendo las recomendaciones de las arquitecturas hexagonales, la clave es separar la lógica de
negocio, el modelo del dominio, de los elementos externos; repositorios de datos, apis rests, etc. 
Los agentes externos serían los puertos y se comunicarían con los elemntos del dominio por medio de
adaptadores. Con esto se consigue ser independiente de las herramientas y los elementos externos.

En el ejemplo de la prueba, migrar la aplicación a Flask u otro framework python significaría,
prácticamente, reescribir la aplicación de 0. En el caso de arquitectura hexagonal, se cambiaría un
puerto por otro y se escribiría un nuevo adaptador, permaneciendo constante el modelo del dominio.
